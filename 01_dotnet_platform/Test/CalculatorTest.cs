using Utils;
using Xunit;

namespace Test
{
    public class CalculatorTest
    {
        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(1, 1, 2)]
        [InlineData(1, 9, 10)]
        public void SimpleAddition(int a, int b, int res)
        {
            var calculator = new Calculator(a, b);
            Assert.Equal(res, calculator.Add());
        }

        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(1, 1, 0)]
        [InlineData(1, 9, -8)]
        [InlineData(9, 1, 8)]
        public void SimpleSubtraction(int a, int b, int res)
        {
            var calculator = new Calculator(a, b);
            Assert.Equal(res, calculator.Sub());
        }

        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(1, 1, 1)]
        [InlineData(1, 9, 9)]
        [InlineData(2, 3, 6)]
        public void SimpleMultiplication(int a, int b, int res)
        {
            var calculator = new Calculator(a, b);
            Assert.Equal(res, calculator.Mul());
        }

        [Theory]
        [InlineData(1, 1, 1)]
        [InlineData(4, 2, 2)]
        public void SimpleDivision(int a, int b, int res)
        {
            var calculator = new Calculator(a, b);
            Assert.Equal(res, calculator.Div());
        }
    }
}