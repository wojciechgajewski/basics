# .NET Platform

For convenient navigation:

```
export ROOT=$PWD
```

How to add console application to solution?

```
cd $ROOT/
dotnet new console -o App

cd $ROOT/App
dotnet run
```

Remember to ignore not needed files

```
cd $ROOT/
echo "bin/" > .gitignore
echo "obj/" >> .gitignore
```

How to add library to solution?

```
cd $ROOT/
dotnet new classlib -o Utils

cd $ROOT/Utils
dotnet build
```

How to add unit tests to solution?

```
cd $ROOT/
dotnet new xunit -o Test

cd $ROOT/Test
dotnet test
```

Now all projects are created as separate parts of solution.
Now we need to bind them in order to use **Utils** in **App** and **Test**:

```
cd $ROOT/App
dotnet add reference $ROOT/Utils
dotnet run

cd $ROOT/Test
dotnet add reference $ROOT/Utils
dotnet test
```

How to create empty solution?

```
cd $ROOT/
dotnet new sln
```

Also the solution and projects are not yet connected.

```
cd $ROOT/

dotnet sln add Utils
dotnet sln add App
dotnet sln add Test
```

Now commands can be executed from solution directory:

```
cd $ROOT/

dotnet build
dotnet test
dotnet run --project App
```

For many useful inspection we can install additional FxCop package:

```
cd $ROOT/Utils
dotnet add package Microsoft.CodeAnalysis.FxCopAnalyzers --version 2.9.4

cd $ROOT/App
dotnet add package Microsoft.CodeAnalysis.FxCopAnalyzers --version 2.9.4

cd $ROOT/Test
dotnet add package Microsoft.CodeAnalysis.FxCopAnalyzers --version 2.9.4
```

And for the rules to be enforced let's treat all warnings as errors:

```xml
<Project>
  <PropertyGroup>
    <TreatWarningsAsErrors>true</TreatWarningsAsErrors>
    <NoWarn>CA1303</NoWarn>
  </PropertyGroup>
</Project>
```

Change should be added in *.csproj files.
Disabled warning CA1303 is for localization of strings.

It is also a good idea to use latest and greatest features from C# 8.0 (and preview) with nullable references (and ranges):
```xml
<Project>
  <PropertyGroup>
    <LangVersion>preview</LangVersion>
    <Nullable>enable</Nullable>
  </PropertyGroup>
</Project>
```

For libraries version of TargetFramework framework can be updated to 2.1 (latest preview):
```xml
  <PropertyGroup>
    <TargetFramework>netstandard2.1</TargetFramework>
  </PropertyGroup>
```
