using System;
using System.IO;
using Microsoft.Extensions.Logging;
using Moq;
using Utils;
using Xunit;

namespace Test
{
    public class DemoTest
    {
        [Fact]
        public void LoginInfo()
        {
            var mock = new Mock<TextWriter>();
            Console.SetOut(mock.Object);

            const string name = "John";
            mock.Setup(tw => tw.WriteLine($"Hello {name}!"));

            var mock2 = new Mock<ILogger<Demo>>();
            ILogger logger = mock2.Object;

            var demo = new Demo("John", logger);
            demo.LoginInfo();
        }

        [Fact]
        public void Run()
        {
            var mock = new Mock<TextWriter>();
            Console.SetOut(mock.Object);

            const string name = "John";
            mock.Setup(tw => tw.WriteLine($"Hello {name}!"));

            //logger
            var mock2 = new Mock<ILogger<Demo>>();
            ILogger logger = mock2.Object;

            var demo = new Demo("John", logger);
            demo.Run();
        }
    }
}