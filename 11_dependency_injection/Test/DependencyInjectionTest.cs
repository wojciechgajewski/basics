using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Test
{
    internal interface IFoo
    {
        int Test();
    }

    internal class Foo : IFoo
    {
        public int Test()
        {
            return 7;
        }
    }

    internal class Bar
    {
        private readonly IFoo _foo;

        public Bar(IFoo foo)
        {
            _foo = foo;
        }

        public int Test()
        {
            return _foo.Test();
        }
    }

    public class DependencyInjectionTest
    {
        [Fact]
        public void AddScoped()
        {
            var serviceProvider = new ServiceCollection().AddScoped<Foo>().BuildServiceProvider();
            var foo = serviceProvider.CreateScope();
            var foo1 = foo.ServiceProvider.GetService<Foo>();
            var foo2 = foo.ServiceProvider.GetService<Foo>();
            foo.Dispose();

            Assert.Same(foo1, foo2);
        }

        [Fact]
        public void DiffrentScopedSecond()
        {
            var serviceProvider = new ServiceCollection().AddScoped<Foo>().BuildServiceProvider();

            var fo1 = serviceProvider.CreateScope();
            var fo2 = serviceProvider.CreateScope();

            var foo1 = fo1.ServiceProvider.GetService<Foo>();
            var foo2 = fo2.ServiceProvider.GetService<Foo>();
            fo1.Dispose();
            fo2.Dispose();
            Assert.NotSame(foo1, foo2);
        }

        [Fact]
        public void HasSomeTypesForTesting()
        {
            var foo = new Foo();
            var bar = new Bar(foo);
            Assert.Equal(7, bar.Test());
        }

        [Fact]
        public void Injections()
        {
            var serviceProvider = new ServiceCollection().AddTransient<IFoo, Foo>().BuildServiceProvider();

            var foo1 = serviceProvider.GetService<Foo>(); // An instance of Foo
            var foo2 = serviceProvider.GetService<IFoo>(); // An instance of Foo
            Assert.NotSame(foo1, foo2);
        }

        [Fact]
        public void Singletons()
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<Foo>()
                .AddSingleton<Bar>()
                .BuildServiceProvider();

            var foo1 = serviceProvider.GetService<Foo>();
            var foo2 = serviceProvider.GetService<Foo>();

            Assert.Same(foo1, foo2);
        }
    }
}