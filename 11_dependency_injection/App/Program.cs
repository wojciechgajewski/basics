﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Utils;

namespace App
{
    internal static class Program
    {
        [ExcludeFromCodeCoverage]
        private static void Main()
        {
            //  var demo = new Demo("John");
            //  demo.Run();
            var serviceCollection = new ServiceCollection();
            ConfigureService(serviceCollection); 
      //      var serviceProvider = serviceCollection.BuildServiceProvider();

            var loger = new Loging();
            var demo = new Demo("John", loger.Loger());
            demo.LoginInfo();
            demo.Run();
            loger.LogerDispose();
        }

        private static void ConfigureService(IServiceCollection services)
        {
            services.AddLogging(configure => configure.AddConsole()).AddTransient<Demo>();
        }
    }
}