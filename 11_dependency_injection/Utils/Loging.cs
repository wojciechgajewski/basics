using Microsoft.Extensions.Logging;

namespace Utils
{
    public class Loging
    {
        private readonly ILoggerFactory _loggerFactory = LoggerFactory.Create(builder => builder
            .AddConsole());

        public ILogger Loger()
        {
            ILogger ilogger = _loggerFactory.CreateLogger<Demo>();
            return ilogger;
        }

        public void LogerDispose()
        {
            _loggerFactory.Dispose();
        }
    }
}