﻿using System;
using Microsoft.Extensions.Logging;
// ReSharper disable All

namespace Utils
{
    public class Demo
    {
        private readonly ILogger _ilogger;
        private readonly string _name;

        public Demo(string name, ILogger ilogger)
        {
            _name = name;
            _ilogger = ilogger;
        }

        public void LoginInfo()
        {
            _ilogger.LogInformation("Login Panel");
        }

        public void Run()
        {
            Console.WriteLine($"Hello {_name}!");
        }
    }
}