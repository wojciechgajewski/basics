using System.IO;
using Modeling;
using Moq;
using Xunit;

namespace Test
{
    public class FileSeismogramTest
    {
        [Fact]
        public void Close()
        {
            var mock = new Mock<TextWriter>(MockBehavior.Strict);
            var fileSource = new FileSeismogram(mock.Object);

            mock.Setup(tw => tw.Close()).Verifiable();

            fileSource.Close();

            mock.Verify();
        }

        [Fact]
        public void StoreWritesToFile()
        {
            var mock = new Mock<TextWriter>(MockBehavior.Strict);
            var fileSource = new FileSeismogram(mock.Object);

            const double time = 10.1;
            const double value = 12.2;

            mock.Setup(tw => tw.WriteLine("10.1 12.2")).Verifiable();
            fileSource.Store(time, value);

            mock.Verify();
        }
    }
}