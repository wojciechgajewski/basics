namespace Modeling
{
    public class Simulation
    {
        private readonly ISeismogram _seismogram;
        private readonly ISource _source;

        public Simulation(ISource source, ISeismogram seismogram)
        {
            _source = source;
            _seismogram = seismogram;
        }

        public void Execute(double start, double step, double stop)
        {
            for (var t = start; step > 0 ? t <= stop : t >= stop; t += step)
            {
                var value = _source.Sample(t);

                // You might do the wave field modeling here :)

                _seismogram.Store(t, value);
            }
        }
    }
}