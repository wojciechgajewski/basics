using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace Modeling
{
    public class FileSource : ISource
    {
        private readonly List<Data> _data = new List<Data>();

        public FileSource(TextReader? textReader)
        {
            if (textReader == null)
                throw new ArgumentNullException(nameof(textReader));

            string line;
            while ((line = textReader.ReadLine()) != null)
            {
                var items = line.Split();

                if (items.Length != 2)
                    throw new InvalidFileException("Line should contain two values");

                var format = CultureInfo.InvariantCulture.NumberFormat;

                var time = float.Parse(items[0], format);
                var value = float.Parse(items[1], format);

                _data.Add(new Data(time, value));
            }

            _data.Sort((lhs, rhs) => lhs.Time.CompareTo(rhs.Time));
        }

        public double Sample(double time)
        {
            if (_data.Count == 0)
                return 0;

            for (var i = 0; i < _data.Count - 1; i++)
                if (_data[i].Time <= time && _data[i + 1].Time >= time)
                    return Interpolate(_data[i], _data[i + 1], time);

            return _data[0].Time >= time ? _data[0].Value : _data[^1].Value;
        }

        private static double Interpolate(Data a, Data b, double time)
        {
            return a.Value + (b.Value - a.Value) / (b.Time - a.Time) * (time - a.Time);
        }

        private struct Data
        {
            public readonly double Time;
            public readonly double Value;

            public Data(double time, double value)
            {
                Time = time;
                Value = value;
            }
        }
    }
}