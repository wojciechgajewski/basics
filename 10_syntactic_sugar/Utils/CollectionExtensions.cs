﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;

namespace Utils
{
    public static class CollectionExtensions
    {
        public static (int min , int max) MinMax(this IEnumerable<int>? enumerable)
        {
			if (enumerable == null)
				throw new ArgumentNullException(nameof(enumerable));
			else if (enumerable?.Count() == 0) return (int.MaxValue, int.MinValue);

			int min = enumerable.Min();
			int max = enumerable.Max();
		  
			return (min,max);
        }

        public static (double avg, double stdDev) AvgStdDev(this IEnumerable<int>? enumerable)
        {
            if (enumerable == null)
                throw new ArgumentNullException(nameof(enumerable));

            double avg = enumerable.Average();
            
            
            double stdDev = 0;
            foreach (var element in enumerable)
            {
                stdDev+=Math.Pow(element - avg, 2);
            }

            stdDev /= enumerable.Count();
            stdDev = Math.Sqrt(stdDev);

            return (avg, stdDev);
        }
    }
}