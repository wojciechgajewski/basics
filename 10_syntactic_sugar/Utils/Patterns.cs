namespace Utils
{
    public enum Result
    {
        Null,
        EmptyString,
        ShortString,
        FiveCharacterString,
        LongString,
        Point2D,
        Point2DOrigin,
        Point2DSumOfCoordinatesIsEven,
        Unknown
    }

    public static class Patterns
    {
        public static Result Match(object o)
        {
            return o switch
            {
                null => Result.Null,
                "" => Result.EmptyString,
                
                string s when s.Length <5  => Result.ShortString, 
                string s when s.Length ==5 => Result.FiveCharacterString,
                string s when s.Length >5 => Result.LongString,
                
                (int a, int b) when a==0 && b==0 => Result.Point2DOrigin,
                (int a, int b)  when (a+b)%2 == 0 => Result.Point2DSumOfCoordinatesIsEven,
                (_,_) => Result.Point2D,
                _ => Result.Unknown
            };
        }
    }
}