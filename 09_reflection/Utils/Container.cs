using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;

namespace Utils
{
    public class Container : IContainer
    {
        private class TypeWithBehaviour
        {
            public TypeWithBehaviour(Type type, bool singleton)
            {
                Type = type;
                Singleton = singleton;
            }

            public Type Type { get; }
            public bool Singleton { get; }
        }

        private readonly Dictionary<Type, Dictionary<string, TypeWithBehaviour>> _mapping =
            new Dictionary<Type, Dictionary<string, TypeWithBehaviour>>();

        private readonly Dictionary<Type, Dictionary<string, object>> _singletons =
            new Dictionary<Type, Dictionary<string, object>>();

        public void Map(Type from, Type into, string name = "", bool singleton = false)
        {
            if (from == null) throw new ArgumentNullException(nameof(from));
            if (into == null) throw new ArgumentNullException(nameof(into));

            if (!into.IsSubclassOf(from))
                throw new Exception($"Cannot map from {from.Name} into {into.Name}!");

            if (!_mapping.ContainsKey(from))
                _mapping[from] = new Dictionary<string, TypeWithBehaviour>();

            _mapping[from][name] = new TypeWithBehaviour(into, singleton);
        }

        public void Map<TFrom, TInto>(string name = "", bool singleton = false) where TFrom : class where TInto : TFrom
        {
            Map(typeof(TFrom), typeof(TInto), name, singleton);
        }

        public object? Create(Type? type, string name = "")
        {
   
            if (type == null)
            {         // DONE: Throw when type is null
                throw new ArgumentNullException(nameof(type));
            }
       
            // DONE: Find mapped type and whether it is a singleton or not

            // DONE: If it is a singleton and already created then return it

            // chyba DOne: Get type constructor

            // chyba DOne: Get constructor parameters

            // DONE: Recursively create constructor parameters - call or every one Create(...)
            
            // DONE: Invoke constructor with created parameters 

            // TODO: Save object if it is a singleton 
            
           //  TODO: Return object
            
           if (_mapping.ContainsKey(type))
            {
                if (_singletons.ContainsKey((type)))
                {
                    return _singletons.First(s => s.Key == type);
                }
                var nameMappings = _mapping[type];
                if (nameMappings.ContainsKey(name))
                {
                    var typeWithBehavior = nameMappings[name];
                    var mappedType = typeWithBehavior.Type;
                    var constructors =
                        mappedType.GetConstructors();//(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                    var i = 0;
                    foreach (var constructor in constructors)
                    {
                        if (constructor.IsPrivate)
                        {
                            throw new Exception(message: "Cannot find constructor!");
                        }

                        var parameters = new object?[constructor.GetParameters().Length];
                        foreach (var parameter in constructor.GetParameters())
                        {
                            parameters[i++] = Create(parameter.ParameterType, parameter.Name);
                        }
                        
                        try
                        {
                            return constructor.Invoke(parameters);

                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            throw;
                        }
                    }
                }
            }
           
           
            return null;
        }

        public T? Create<T>(string name = "") where T : class
        {
            return Create(typeof(T), name) as T;
        }
    }
}