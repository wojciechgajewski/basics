using System.Collections.Generic;

namespace Utils
{
    public static class FilterExtension
    {
        public static IEnumerable<int> Even(this IEnumerable<int>? input)
        {
            if (input == null)
                yield break;

            foreach (var value in input)
                if (value % 2 == 0)
                    yield return value;
        }

        public static IEnumerable<int> Odd(this IEnumerable<int>? input)
        {
            if (input == null)
                yield break;

            foreach (var value in input)
                if (value % 2 == 1)
                    yield return value;
        }

        public static IEnumerable<int> Only(this IEnumerable<int>? input, int count)
        {
            if (input == null)
                yield break;

            var i = 0;
            foreach (var value in input)
            {
                if (i++ == count)
                    break;

                yield return value;
            }
        }
    }
}