using System.Linq;
using Utils;
using Xunit;

namespace Test
{
    public class LinqTest
    {
        public LinqTest()
        {
            _numbers = new[] {7, 3, 4, 1, 3, 2, 3, 5, 4, 4, 5, 5, 6, 7};
            _words = new[] {"house", "plane", "snow", "dog", "cat", "pizza", "dotnet", "space"};

            _users = new[]
            {
                new User {Name = "John", Age = 12},
                new User {Name = "Tom", Age = 60},
                new User {Name = "Tim", Age = 22},
                new User {Name = "Stephan", Age = 100},
                new User {Name = "Peter"},
                new User {Age = 112},
                new User()
            };

            _posts = new[]
            {
                new Post(_users[0].Id) {Title = "invalid"},
                new Post(_users[0].Id) {Title = "Valid"},
                new Post(_users[1].Id) {Title = "invalid"},
                new Post(_users[2].Id) {Title = "invalid"},
                new Post(_users[3].Id) {Title = "Valid"},
                new Post(_users[3].Id) {Title = ""},
                new Post(_users[3].Id)
            };

            _comments = new[]
            {
                new Comment(_posts[0].Id) {Title = "Well done!", Text = "This it the best!"},
                new Comment(_posts[0].Id) {Title = "Mediocre...", Text = "You should try harder..."},
                new Comment(_posts[5].Id) {Title = "Hmm?", Text = "Missing title?"},
                new Comment(_posts[6].Id) {Title = "Why no title?", Text = "Isn't this invalid?"},
                new Comment(_posts[4].Id) {Title = "This is very long title.", Text = "This is short."}
            };
        }

        private readonly int[] _numbers;
        private readonly string[] _words;
        private readonly User[] _users;
        private readonly Post[] _posts;
        private readonly Comment[] _comments;

        [Fact]
        public void SelectAdultUsers()
        {
            // TODO: ...

            Assert.Equal(new[] {_users[1], _users[2], _users[3], _users[5]}, result);
        }

        [Fact]
        public void SelectAverageUserNameLengthForUsersWhoHaveName()
        {
            // TODO: ...

            Assert.Equal(4.4, result, 2);
        }

        [Fact]
        public void SelectCommentsWhereTitleIsLongerThanText()
        {
            // TODO: ...

            Assert.Equal(new[] {_comments[4]}, result);
        }

        [Fact]
        public void SelectOnlyUserNameForAdultUsers()
        {
            // TODO: ...

            Assert.Equal(new[] {_users[1].Name, _users[2].Name, _users[3].Name, _users[5].Name}, result);
        }

        [Fact]
        public void SelectSumOfEvenNumbersAndSumOfOddNumbers()
        {
            // TODO: ...

            var resultArray = result.ToArray();
            Assert.Equal(2, resultArray.Length);
            Assert.Equal(resultArray[0], (even: false, sum: 39));
            Assert.Equal(resultArray[1], (even: true, sum: 20));
        }

        [Fact]
        public void SelectTotalNumberOfCharactersInAllWords()
        {
            // TODO: ...

            Assert.Equal(36, result);
        }

        [Fact]
        public void SelectUserCommentsForPostsWithNoTitle()
        {
            // TODO: ...

            var resultArray = result.ToArray();
            Assert.Equal(2, resultArray.Length);
            Assert.Equal(resultArray[0], (_users[3], _comments[2]));
            Assert.Equal(resultArray[1], (_users[3], _comments[3]));
        }

        [Fact]
        public void SelectUsersWherePostTitleStartsWithUppercaseLetter()
        {
            // TODO: ...

            Assert.Equal(new[] {_users[0], _users[3]}, result);
        }

        [Fact]
        public void SelectUsersWithNameLongerThanThreeCharacters()
        {
            // TODO: ...

            Assert.Equal(new[] {_users[0], _users[3], _users[4]}, result);
        }

        [Fact]
        public void SelectUserWithNameTom()
        {
            // TODO: ...

            Assert.Equal(new[] {_users[1]}, result);
        }

        [Fact]
        public void SelectThreeLongestWordsInDescendingOrder()
        {
            // TODO: ...
            
            Assert.Equal(new []{"dotnet", "house", "plane"}, result);
        }
    }
}