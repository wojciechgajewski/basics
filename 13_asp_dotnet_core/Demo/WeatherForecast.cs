using System;

namespace Demo
{
    public class WeatherForecast
    {
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        // ReSharper disable once UnusedMember.Global
        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public string? Summary { get; set; }
    }
}
