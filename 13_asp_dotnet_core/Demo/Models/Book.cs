

using System.ComponentModel.DataAnnotations;

#pragma warning disable
namespace Demo.Models
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class Book
    {
        [Key]
        public int Id { get; set; }
        
        public string? Title { get; set; }
        
        public string? Author { get; set; }
    }
}