import React, { Component } from 'react';
import authService from './api-authorization/AuthorizeService'

export class Books extends Component {
    static displayName = Books.name;

    constructor(props) {
        super(props);
        this.state = { books: [], loading: true };
    }

    componentDidMount() {
        this.populateBooksData();
    }

    static renderBooksTable(books) {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Author</th>
                    </tr>
                </thead>
                <tbody>
                    {books.map(books =>
                        <tr key={books.Id}>
                            <td>{books.id}</td>
                            <td>{books.title}</td>
                            <td>{books.author}</td>
                        </tr>
                    )}
            </tbody>
        </table>
    );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
    : Books.renderBooksTable(this.state.books);

        return (
            <div>
            <h1 id="tabelLabel" >Books</h1>
        <p>This component demonstrates getting books from database.</p>
        {contents}
    </div>
    );
    }

    async populateBooksData() {
        const token = await authService.getAccessToken();
        const response = await fetch('books', {
            headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
        });
        const data = await response.json();
        this.setState({ books: data, loading: false });
    }
}
