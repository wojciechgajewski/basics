using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class StackTest
    {
        [Fact]
        public void StackOfIntBasicOperations()
        {
            var queue = new Stack<int>();

            queue.Push(1);
            queue.Push(2);
            queue.Push(3);

            int Next()
            {
                return queue.Pop();
            }

            Assert.Equal(3, queue.Count);

            Assert.Equal(3, Next());
            Assert.Equal(2, Next());
            Assert.Equal(1, Next());

            Assert.Empty(queue);
        }
    }
}