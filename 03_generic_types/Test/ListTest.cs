using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class ListTest
    {
        [Fact]
        public void ListOfIntHasSometimesHigherCapacityThanCount()
        {
            var list = new List<int> {1};

            Assert.Single(list);
            Assert.Equal(4, list.Capacity);
        }

        [Fact]
        public void ListOfIntHasSometimesTheSameSizeAndCapacity()
        {
            var listOne = new List<int> {2, 4, 5, 6};

            Assert.Equal(4, listOne.Count);
            Assert.Equal(4, listOne.Capacity);

            var listTwo = new List<int> {2, 4};

            Assert.Equal(2, listTwo.Count);
            Assert.Equal(4, listTwo.Capacity);
        }

        [Fact]
        public void ListOfIntSometimesGrowsCapacityAfterAdd()
        {
            var list = new List<int> {2, 4};

            Assert.Equal(2, list.Count);
            Assert.Equal(4, list.Capacity);

            list.Add(6);

            Assert.Equal(3, list.Count);
            Assert.Equal(4, list.Capacity);

            list.Add(6);

            Assert.Equal(4, list.Count);
            Assert.Equal(4, list.Capacity);

            list.Add(6);

            Assert.Equal(5, list.Count);
            Assert.Equal(8, list.Capacity);
        }

        [Fact]
        public void ListOfStringCanBeCreatedInOneLine()
        {
            var list = new List<string> {"Foo", "Bar", "Baz"};

            Assert.Equal(3, list.Count);
            Assert.Equal("Foo", list[0]);
            Assert.Equal("Bar", list[1]);
            Assert.Equal("Baz", list[2]);
        }
    }
}