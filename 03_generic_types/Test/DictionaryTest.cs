using System;
using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class DictionaryTest
    {
        [Fact]
        public void DictionaryOfInToIntChangeExistingValue()
        {
            var dictionary = new Dictionary<int, int> {{1, 10}};

            Assert.Equal(10, dictionary[1]);

            dictionary[1] = 20;

            Assert.Equal(20, dictionary[1]);
        }

        [Fact]
        public void DictionaryOfInToIntRemoveValues()
        {
            var dictionary = new Dictionary<int, int> {{1, 10}, {2, 20}, {3, 30}};

            Assert.True(dictionary.ContainsKey(1));
            Assert.True(dictionary.ContainsKey(2));
            Assert.True(dictionary.ContainsKey(3));

            dictionary.Remove(2);
            dictionary.Remove(3);

            Assert.True(dictionary.ContainsKey(1));
            Assert.False(dictionary.ContainsKey(2));
            Assert.False(dictionary.ContainsKey(3));
        }

        [Fact]
        public void DictionaryOfInToIntTryGetValue()
        {
            var dictionary = new Dictionary<int, int> {{1, 10}};

            Assert.Equal(10, dictionary[1]);

            var hasValue = dictionary.TryGetValue(1, out var value);

            Assert.True(hasValue);
            Assert.Equal(10, value);
        }

        [Fact]
        public void DictionaryOfIntToIntCanBeCreatedInOneLine()
        {
            var dictionary = new Dictionary<int, int> {{1, 2}, {3, 4}};

            Assert.Equal(2, dictionary[1]);
            Assert.Equal(4, dictionary[3]);
        }

        [Fact]
        public void DictionaryOfStringToStringThrowsWheKeyNotInDictionary()
        {
            var dictionary = new Dictionary<string, string> {{"A", "B"}};

            Assert.Equal("B", dictionary["A"]);

            void AccessElement()
            {
                var unused = dictionary["C"];
            }

            Assert.Throws<KeyNotFoundException>(AccessElement);

            dictionary["C"] = "X";

            AccessElement();
        }

        [Fact]
        public void DictionaryOfStringToStringThrowsWhenAddedElementExists()
        {
            var dictionary = new Dictionary<string, string> {{"A", "B"}};

            Assert.Equal("B", dictionary["A"]);

            void AddElement()
            {
                dictionary.Add("A", "C");
            }

            Assert.Throws<ArgumentException>(AddElement);

            dictionary.Remove("A");

            AddElement();
            Assert.Equal("C", dictionary["A"]);
        }
    }
}