﻿using System.Collections;
using System.Collections.Generic;

namespace Utils
{
    public class Collection<T> : IEnumerable<T>
    {
        private T[] _elements;

        public Collection(int capacity = 1, int factor = 2)
        {
            Capacity = capacity;
            Factor = factor;
            _elements = new T[capacity];
        }

        public int Count { get; private set; }
        public int Capacity { get; private set; }
        public int Factor { get; }

        public T this[int i] => _elements[i];

        public IEnumerator<T> GetEnumerator()
        {
            for (var i = 0; i < Count; i++) yield return _elements[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T element)
        {
            if (_elements.Length == Count)
            {
                var newElements = new T[Count * Factor];
                _elements.CopyTo(newElements, 0);
                _elements = newElements;
                Capacity = _elements.Length;
            }

            _elements[Count++] = element;
        }
    }
}