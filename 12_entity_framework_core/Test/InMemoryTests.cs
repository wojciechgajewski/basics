using System;
using System.Linq;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.InMemory.Storage.Internal;
using Utils;
using Xunit;
using AppContext = Utils.AppContext;

namespace Test
{
    public class InMemoryTests
    {
        [Fact]
        public void UseInMemory()
        {
            var options = new DbContextOptionsBuilder<AppContext>()
                .UseInMemoryDatabase(databaseName: "Add_writes_to_database")
                .Options;
            const int id = 1;
            const string name = "N";
            const string description = "D";
            var data = DateTime.Today;
            var uri = new Uri("http://example.com");

            // Run the test against one instance of the context
            using (var context = new AppContext(options))
            {
                var project = new Project {
                    Id = id,
                    Name = name,
                    Description = description,
                    CreationDate = data,

                    // TODO: Enable in second exercise...
                    Uri = uri
                };
                context.Add(project);
                context.SaveChanges();
                Assert.Equal(name, project.Name);
            }
        }

        [Fact]
        public void DataSourceMemory()
        {
            // In-memory database only exists while the connection is open
                var connection = new SqliteConnection("DataSource=:memory:");
                connection.Open();

                try
                {
                    var options = new DbContextOptionsBuilder<AppContext>()
                        .UseSqlite(connection)
                        .Options;

                    // TODO: Enable in second exercise...
                    var uri = new Uri("http://example.com");

                    // Create the schema in the database
                    using (var context = new AppContext(options))
                    {
                        context.Database.EnsureCreated();
                    }

                    // Run the test against one instance of the context
                    using (var context = new AppContext(options))
                    {
                        var project = new Project
                        {
                            Name = "Nowy projekt",
                            Description = "pierwszy projekt",
                            CreationDate = DateTime.Now,
                            Uri = null

                        };
                    //    project.Add("https://example.com");
                    //    context.SaveChanges();
                    Assert.Null(project.Uri);
                    }

                    // Use a separate instance of the context to verify correct data was saved to database

                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }
    