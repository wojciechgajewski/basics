using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
//using Utils;

namespace Utils
{
    public class AppContext : DbContext
    {
        public DbSet<Project> Projects { get; set; } = null!;
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var sqlitePath = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), 
                @"02_entity_framework_core\App.db");

            if (optionsBuilder is null)
                throw new System.ArgumentNullException(paramName:null);
            //inMemory
     if (!optionsBuilder.IsConfigured)
     {
       //  optionsBuilder.UseSqlite(@"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0");
       optionsBuilder.UseSqlite("Data Source=" + sqlitePath + "\\App.db;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0");
     }
            //App.db
//            var sqlitePath = Path.Combine(
//                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), 
//                @"02_entity_framework_core\App.db");
            
        //    optionsBuilder.UseSqlite("Data Source=" + sqlitePath + "\\App.db");
        }
        public AppContext()
        { }
        public AppContext(DbContextOptions<AppContext> options)
            : base(options)
        { }
    }
}