﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Utils;
using AppContext = Utils.AppContext;

#pragma warning disable CA1062
namespace App
{
    [ExcludeFromCodeCoverage]
    internal static class Program
    {
        private static void Main()
        {
            // TODO: Create and use DB context...
            using (var context = new AppContext())
            {
                var proj1 = new Project
                {
                    Name = "Nowy projekt",
                    Description = "pierwszy projekt",
                    CreationDate = DateTime.Now,
                    Uri = null
                    
                };
                context.Add(proj1);
                context.SaveChanges();
            }

            using (var context = new AppContext())
            {
                var proj = context.Projects.First(p => p.Id == 2);
                Console.WriteLine($"Added project : {proj.Id} : {proj.Name}, {proj.Description}, {proj.CreationDate}, {proj.Uri}");
            }
            Console.WriteLine("Hello World!");
        }
    }
}